#!/bin/sh

echo This script creates a self-signed certificates for access.  For real certificates, add these to the nginx container configuration.

if [ -f /mycerts/traefik.crt  ]; then \
    echo Failed: this script will not overwrite existing certificates.  Remove the existing certs from the ./certs directory and then rerun this command.
    exit 0
fi

openssl req -x509 -nodes -newkey rsa:2048 -keyout /mycerts/traefik.key -out /mycerts/traefik.crt
