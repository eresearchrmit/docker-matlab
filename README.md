
# Docker MATLAB

## Installation

* Install recent docker-engine and docker-compose from [http://docker.com](www.docker.com).

* Get this repostory:

        git clone https://bitbucket.org/eresearchrmit/docker-matlab
        cd docker-matlab

## Configuration

* Copy environment variable template and edit as needed (change the default password!)

        cp env .env

* Generate self-signed SSL certicates:

        docker-compose build makecerts
        docker-compose run makecerts

* Start the system

        docker-compose build
        docker-compose up -d



## Usage

The system should be available at `https://$HOSTNAME` where `$HOSTNAME` is the ip address of the host.  Once you login into the graphical interface,  you have a full linux desktop with data persisted in directory `./data` on the host.

You can then install the Linux MATLAB by downloading from Mathworks the usual way via the included firefox browser.

If you want to change the password (after initial setting in `.env`), use the `vncpassword` command from within a terminal window

## Licenses

* NoVNC is MPL 2.0 (Mozilla Public License 2.0)
